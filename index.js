let tower1 = document.querySelector("#tower1")
let tower2 = document.querySelector("#tower2")
let tower3 = document.querySelector("#tower3")
let tower = document.querySelector(".tower")
let selectedItem 
let isSelected = false


tower1.addEventListener("click", firstMove)
tower2.addEventListener("click", firstMove)
tower3.addEventListener("click", firstMove)

tower1.addEventListener("click", secondMove)
tower2.addEventListener("click", secondMove)
tower3.addEventListener("click", secondMove)

function firstMove() {
    if (!isSelected){
    selectedItem = event.currentTarget.lastElementChild 
    isSelected = true
    selectedItem.style.border = "dashed"
    }
}

function secondMove () {
    let topBlock = event.currentTarget.lastElementChild
    if(isSelected){
        if(!topBlock) {
            event.currentTarget.appendChild(selectedItem)
            gameOver()
            isSelected = false
            selectedItem.style.border = "solid #345"
            selectedItem = undefined 
            
        }
        else if(selectedItem.clientWidth < topBlock.clientWidth){
            event.currentTarget.appendChild(selectedItem)
            gameOver()
            isSelected = false
            selectedItem.style.border = "solid #345"
            selectedItem = undefined
        
        }
        else if(selectedItem.clientWidth > topBlock.clientWidth){
            isSelected = false
            selectedItem.style.border = "solid #345"
            selectedItem = undefined
        }
    }

}

function gameOver(){
    if (tower3.childElementCount === 4){
        setTimeout(() => alert("You Win!!!"))
    }
}
